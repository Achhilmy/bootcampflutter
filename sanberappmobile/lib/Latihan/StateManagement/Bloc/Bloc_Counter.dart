import 'dart:async';

import 'dart:developer';

import 'package:sanberappmobile/Latihan/StateManagement/Bloc/EventManager.dart';

class Bloc_Counter{
  int _counter = 0;
  final _counterStateController = StreamController<int>();

  StreamSink<int> get _inCounter => _counterStateController.sink;

  Stream<int> get counter => _counterStateController.stream;

   final _counterEventController = StreamController<EventManager>();

   Sink<EventManager>get counterEventSink => _counterEventController.sink;

   Bloc_Counter(){
     _counterEventController.stream.listen(_mapEventtoState);
   }
   void _mapEventtoState(EventManager event){
     if (event is IncrementEvent)
     _counter ++;
     else 
     _counter --;

     _inCounter.add(_counter);
   }

   void dispose(){
     _counterEventController.close();
     _counterStateController.close();
   }
}