import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:sanberappmobile/Latihan/StateManagement/Bloc/MainApp.dart';
import 'package:sanberappmobile/Latihan/StateManagement/local_counter.dart';
//import 'package:sanberappmobile/Tugas/Tugas15/HomeScreen.dart';
import 'Latihan/Authentication/LoginScreen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MainApp(),
    );
  }
}

