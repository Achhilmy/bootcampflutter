import 'bangun_datar.dart';

class Segitiga extends BangunDatar {
  double alas;
  double tinggi;

  Segitiga(double alas, double tinggi){
    this.alas = alas;
    this.tinggi = tinggi;
  }

  @override 
  double luas(){
    return this.alas * this.tinggi;
  }
}