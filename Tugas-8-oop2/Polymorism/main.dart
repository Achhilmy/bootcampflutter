
import 'bangun_datar.dart';
import 'lingkaran.dart';
import 'persegi.dart';
import 'segitiga.dart';

void main(List<String> args) {
  BangunDatar bangunDatar = new BangunDatar();
  Persegi persegi = new Persegi(4);
  Segitiga segitiga = new Segitiga(6, 4);
  Lingkaran lingkaran = new Lingkaran(3.14, 50);


  bangunDatar.luas();
  bangunDatar.keliling();

  print("luas persegi: " + persegi.luas().toString());
  print("luas lingkaran: " + lingkaran.luas().toString());
  print("luas segitiga: " + segitiga.luas().toString());

}