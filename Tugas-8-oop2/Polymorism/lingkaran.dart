import 'bangun_datar.dart';

class Lingkaran extends BangunDatar{
  double r;
  double pi;

  Lingkaran (double r, double pi){
    this.r = r;
    this.pi = pi;
  }

  @override 
  double luas(){
    return pi * r * r;
  }

  @override
  double keliling(){
    return 2 * pi * r ;
  }
}