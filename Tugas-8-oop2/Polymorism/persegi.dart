import 'bangun_datar.dart';

class Persegi extends BangunDatar {
  double sisi;

  Persegi(double sisi){
    this.sisi = sisi;
  }
  @override 
  double luas(){
    return this.sisi * this.sisi;
  }
  @override 
  double keliling(){
    return this.sisi *4;
  }
  
}