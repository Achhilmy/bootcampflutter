import 'package:flutter/material.dart';
import 'package:sanberappflutter/Quiz3/LoginScreen.dart';

//import 'package:sanberappflutter/Tugas/Tugas15/LoginScreen.dart';

void main()=>runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginScreen(),
    );
  }
}