import 'package:flutter/material.dart';

class Counter extends StatefulWidget {
  @override
  _CounterState createState() => _CounterState();
}

class _CounterState extends State<Counter> {
  int _counter = 0;
  String _lastAction = "None";

  void _incrementCounter({int increase = 1 }) {
    setState(() {
      _counter+= increase;
      _lastAction = "Increment";
    });
  }

  void _decrementCounter({int decrease = 1}){
    setState(() {
      _counter-= decrease;
      _lastAction = "Decrement";
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Counter Local state")
      ),
      body: Center(
        child: Column(  
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),),
        floatingActionButton: Row(
        mainAxisSize : MainAxisSize.min,
        children: <Widget>[
          FloatingActionButton(
            onPressed: _incrementCounter,
            tooltip: 'Increment',
            child: Icon(Icons.add),
          ), // This trailing comma makes auto-formatting nicer for build methods.
          SizedBox(width : 10.0),
          FloatingActionButton(
            onPressed: _decrementCounter,
            tooltip: 'Decrement',
            child: Icon(Icons.remove),
          ),
        ]
        
      )      
    );
        
  }
}